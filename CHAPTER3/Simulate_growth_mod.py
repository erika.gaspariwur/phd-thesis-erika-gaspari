
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 11 11:24:43 2018
@author: Erika Gaspari
"""



#import section 
import cobra
import pandas
pandas.options.display.max_rows=400
import cobra.test
import libsbml
from cobra import Model, Reaction, Metabolite
from cobra import flux_analysis
f
#read in model 
model = cobra.io.read_sbml_model("iEG158mpnAL7_nadph_gadn.xml")

#set solver
model.solver='gurobi'

#count number of reactions in the model and prevent uptake of nutrients

n_ex=0 
n_t=0
n_grr=0
for i in range(len(model.reactions)):
    if 'EX' in model.reactions[i].id:
        n_ex=n_ex+1
        model.reactions[i].bounds = (0 , 0)
    if 'transport' in model.reactions[i].name:
        n_t=n_t+1
    if 'antiport' in model.reactions[i].name:
        n_t=n_t+1
    if 'export' in model.reactions[i].name:
        n_t=n_t+1
    if model.reactions[i].gene_reaction_rule!='':
        n_grr=n_grr+1
n_c=len(model.reactions)-n_ex-n_t


#define media components and corresponding exchange reactions

#amino acids must be supplied to MPN, these are below
amino_acids=['EX_thr__L_e','EX_leu__L_e','EX_cys__L_e','EX_glu__L_e','EX_asp__L_e',
             'EX_lys__L_e','EX_asn__L_e','EX_ala__L_e','EX_ile__L_e','EX_gly_e',
             'EX_trp__L_e','EX_met__L_e','EX_ser__L_e','EX_his__L_e','EX_val__L_e',
             'EX_gln__L_e','EX_tyr__L_e','EX_phe__L_e','EX_pro__L_e','EX_arg__L_e']

#these are basics in a cell culture - oxygen, carbon dioxide, H+, water, and phosphate
essential_component=['EX_o2_e','EX_co2_e','EX_h_e','EX_h2o_e','EX_pi_e']

#below are essential nutrients, vitamins and cofactors
essential_nutrient=['EX_chol_e','EX_pydx_e','EX_pap_e','EX_fol_e','EX_thm_e','EX_ribflv_e','EX_ptth_e','EX_nac_e']

#these are the simplified fatty acid components
fatty_acid_synthesis=['EX_sphng160_e','EX_sphng161_e','EX_sphng180_e','EX_sphng181_e','EX_sphng182_e','EX_sphng200_e','EX_sphng240_e','EX_clpn181_e','EX_clpn182_e','EX_clpn183_e','EX_hdca_e','EX_hdcea_e','EX_ocdca_e','EX_ocdcea_e','EX_lneldc_e','EX_lnlncg_e','EX_pc160_e','EX_pc161_e','EX_pc180_e','EX_pc181_e','EX_pc182_e','EX_pc183_e']

#nucleotides for RNA and DNA synthesis
nucleotide=['EX_ade_e','EX_cytd_e','EX_gua_e','EX_ura_e','EX_thym_e','EX_dcyt_e']

#these are metabolites that are secreted
export_waste=['EX_lac__L_e','EX_h2o2_e','EX_ac_e','EX_hcys__L_e','EX_nh3_e']

#products - biomass and methylated dna, which is exported from the cell
product=['EX_BM_e','EX_dna5mtc_e']

#carbon/energy source
carbon_source=['EX_glc__D_e']

#these are cofactors and nutrients, but in our model MPN has to be able to get rid of them for the model to be solvable
essential_nutrient_waste=['EX_orn__L_e']

#define set of nutrients in media
fullEssentials = amino_acids + essential_component + fatty_acid_synthesis + essential_nutrient + nucleotide + export_waste + product + carbon_source + essential_nutrient_waste
#allow production and secretion
for comp2 in fullEssentials:
    model.reactions.get_by_id(comp2).lower_bound = -1000
    model.reactions.get_by_id(comp2).upper_bound = 1000

#Setting additional model constraints
model.reactions.get_by_id('ALKP').lower_bound = 0.01
model.reactions.get_by_id('IR08208').lower_bound = 0.01
model.reactions.get_by_id('IR09954').lower_bound = 0.0003489
model.reactions.get_by_id('RNase').lower_bound = 0.007741
model.reactions.EX_arg__L_e.bounds = (-.25,1000)

#set constraints for maintenance reaction
model.reactions.protonLeak.bounds = (0,0)
totalMaintenance = 10.46

#Set constraint for glucose uptake
#Set constraint for glycerol uptake. 
model.reactions.get_by_id('EX_glc__D_e').lower_bound = -5.10835836764991
model.reactions.get_by_id('EX_glyc3p_e').lower_bound = -0.127708959191248
model.reactions.get_by_id('EX_glyc_e').lower_bound = -0.127708959191248
model.reactions.get_by_id('EX_rib__D_e').lower_bound = -0.127708959191248
model.reactions.get_by_id('EX_ac_e').upper_bound = 7.4436
#Set constraint for NH3 uptake. Previously was -10 but the optimization shows 0 is the best one
model.reactions.get_by_id('EX_nh3_e').lower_bound = -10

#below components are limited to very low uptake, since they are not supposed to be carbon sources. increasing these by just a little amount really increases the growth rate, especially pap_c

lowerB = -.02

model.reactions.EX_ade_e.bounds=(lowerB, 1000)
model.reactions.EX_ura_e.bounds=(lowerB,1000)
model.reactions.EX_cytd_e.bounds=(lowerB,1000)
model.reactions.EX_dcyt_e.bounds=(lowerB,1000)
model.reactions.EX_pap_e.bounds=(0,1000)
model.reactions.EX_gua_e.bounds=(lowerB,1000)
model.reactions.EX_thym_e.bounds=(lowerB,1000)

protonFraction = .76

model.reactions.protonLeak.bounds = ( (totalMaintenance * protonFraction * 4) , 1000)
model.reactions.get_by_id('IR08984').lower_bound = totalMaintenance * (1-protonFraction)
model.reactions.G3PO.bounds = (-1000 , .1)
model.reactions.ATPsyn.bounds = (-1000 , 1000)
model.reactions.GLUN.bounds = ( -0.1, 0.1 )

model.reactions.EX_ocdca_e.bounds = (0,0)
model.reactions.EX_ocdcea_e.bounds = (0,0)
model.reactions.pc180lys.bounds = (0,0)
model.reactions.EX_hdca_e.bounds = (0,0)
model.reactions.pc160lys.bounds = (0,0)
model.reactions.THD1.bounds = (0,0)


## Simulating substitution of GADPH: substitution feasible only if leaving normal GADPHi downregulated (upper bounds=8 makes growth=0.017, almost plenty growth with 10model.reactions.PGK.bounds=(0,0)
##model.reactions.PGK.bounds=(0,0)
##model.reactions.RR03680.bounds=(0,0)
##model.reactions.RR09795.bounds=(0,0)
##model.reactions.RR09796.bounds=(0,0)
##model.reactions.GAPDHi.bounds=(-1000,10)
##model.reactions.RR09889.bounds=(0,0)
##model.reactions.GAPDH_MODIFIED.bounds=(0,0)
#model.reactions.MTHFD_1.bounds=(0.001,0.001)


#simulate growth 
modd=model.optimize()
print(modd)
print("The growth rate in the simulation conditions is ", round(modd.objective_value,3))
