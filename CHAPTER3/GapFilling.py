# -*- coding: utf-8 -*-
"""
Created on Wed Nov 22 17:51:13 2017

@author: eryg9
"""

import cobra as cb
import sys
import time
import shutil
from pathlib2 import Path
import pandas
pandas.options.display.max_rows=350
import libsbml
import escher
import json
from escher import Builder
from cobra import Model, Reaction, Metabolite
from cobra import flux_analysis
import random

#Import libraries
import Lib_Phase1 as Lib_1
import Lib_General as Lib_g

New_ID_counter = 0

model = cb.io.read_sbml_model("iEG158_mpn.xml")
            
model.solver='gurobi'

#amino acids must be supplied to MPN, these are below
amino_acids=['EX_thr__L_e','EX_leu__L_e','EX_cys__L_e','EX_glu__L_e','EX_asp__L_e',
             'EX_lys__L_e','EX_asn__L_e','EX_ala__L_e','EX_ile__L_e','EX_gly_e',
             'EX_trp__L_e','EX_met__L_e','EX_ser__L_e','EX_his__L_e','EX_val__L_e',
             'EX_gln__L_e','EX_tyr__L_e','EX_phe__L_e','EX_pro__L_e','EX_arg__L_e']

#these are basics in a cell culture - oxygen, carbon dioxide, H+, water, and phosphate
essential_component=['EX_o2_e','EX_co2_e','EX_h_e','EX_h2o_e','EX_pi_e']

#below are essential nutrients, vitamins and cofactors
essential_nutrient=['EX_chol_e','EX_pydx_e','EX_pap_e','EX_fol_e','EX_thm_e','EX_ribflv_e','EX_ptth_e','EX_nac_e']

#these are the simplified fatty acid components
fatty_acid_synthesis=['EX_sphng160_e','EX_sphng161_e','EX_sphng180_e','EX_sphng181_e','EX_sphng182_e','EX_sphng200_e','EX_sphng240_e','EX_clpn181_e','EX_clpn182_e','EX_clpn183_e','EX_hdca_e','EX_hdcea_e','EX_ocdca_e','EX_ocdcea_e','EX_lneldc_e','EX_lnlncg_e','EX_pc160_e','EX_pc161_e','EX_pc180_e','EX_pc181_e','EX_pc182_e','EX_pc183_e']

#nucleotides for RNA and DNA synthesis
nucleotide=['EX_ade_e','EX_cytd_e','EX_gua_e','EX_ura_e','EX_thym_e','EX_dcyt_e']

#these are metabolites that the cell needs to get rid of
export_waste=['EX_lac__L_e','EX_h2o2_e','EX_ac_e','EX_hcys__L_e','EX_nh3_e']

#products - biomass and methylated dna, which is exported from the cell
product=['EX_BM_e','EX_dna5mtc_c']

#carbon/energy source
carbon_source=['EX_glc__D_e']

#these are cofactors and nutrients, but in our model MPN has to be able to get rid of them for the model to be solvable
essential_nutrient_waste=['EX_orn__L_e']

fullEssentials = amino_acids + essential_component + fatty_acid_synthesis + essential_nutrient + nucleotide + export_waste + product + carbon_source + essential_nutrient_waste

for comp2 in fullEssentials:
    model.reactions.get_by_id(comp2).lower_bound = -30
    model.reactions.get_by_id(comp2).upper_bound = 30

#Setting constraints
model.reactions.get_by_id('ALKP').lower_bound = 0.01
model.reactions.get_by_id('IR08208').lower_bound = 0.01
model.reactions.get_by_id('IR09954').lower_bound = 0.0003489
model.reactions.get_by_id('RNase').lower_bound = 0.007741
model.reactions.EX_arg__L_e.bounds = (-.25,1000)
#set constraints for maintenance reaction
model.reactions.protonLeak.bounds = (0,0)
totalMaintenance = 10.46
#Set constraint for glucose uptake
#Set constraint for glycerol uptake. 
model.reactions.get_by_id('EX_glc__D_e').lower_bound = -5.10835836764991
model.reactions.get_by_id('EX_glyc3p_e').lower_bound = -0.127708959191248
model.reactions.get_by_id('EX_glyc_e').lower_bound = -0.127708959191248
model.reactions.get_by_id('EX_rib__D_e').lower_bound = -0.127708959191248
model.reactions.get_by_id('EX_ac_e').upper_bound = 7.4436
#Set constraint for NH3 uptake. Previously was -10 but the optimization shows 0 is the best one
model.reactions.get_by_id('EX_nh3_e').lower_bound = -10

#below components are limited to very low uptake, since they are not supposed to be carbon sources. increasing these by just a little amount really increases the growth rate, especially pap_c

lowerB = -.02

model.reactions.EX_ade_e.bounds=(lowerB, 1000)
model.reactions.EX_ura_e.bounds=(lowerB,1000)
model.reactions.EX_cytd_e.bounds=(lowerB,1000)
model.reactions.EX_dcyt_e.bounds=(lowerB,1000)
model.reactions.EX_pap_e.bounds=(0,1000)
model.reactions.EX_gua_e.bounds=(lowerB,1000)
model.reactions.EX_thym_e.bounds=(lowerB,1000)


protonFraction = .75

model.reactions.protonLeak.bounds = ( (totalMaintenance * protonFraction * 4) , 1000)
model.reactions.get_by_id('IR08984').lower_bound = totalMaintenance * (1-protonFraction)
model.reactions.G3PO.bounds = (-1000 , .1)
model.reactions.ATPsyn.bounds = (-1000 , 1000)
model.reactions.GLUN.bounds = ( -0.1, 0.1 )


modd = model.optimize()
print modd.objective_value

model.solver = 'gurobi'

database = cb.io.read_sbml_model("REFBiGG.xml")

randomPool = []
for i in range(len(database.reactions)):
    randomPool.append(i)

reference_db = cb.io.read_sbml_model("emptyModel.xml")

while (len(randomPool) > 0):
    rand = random.randrange(len(randomPool))
    randd = randomPool[random.randrange(len(randomPool))]
    reference_db.add_reaction(database.reactions[randd])
    randomPool.remove(randd)

#Filter reference database
Lib_1.delete_overlap(model, reference_db) 

modd = model.optimize()
print modd.objective_value

#Combine model and reference database and initiate MILP
gf = Lib_1.GapFiller(model,universal=reference_db, penalties = dict(universal=1, exchange=100, demand=1))

modd = model.optimize()
print modd.objective_value

for i in range(1,2):
    gf_out = gf.fill(iterations=50, num_reactions=i)
    for sol in gf_out:
        ### Write results to file
        #Open output file
        output = open("output.txt","a") 
    
        #Determine Identifier
        output.write('ID_'+str(New_ID_counter)+'_r'+str(i)+'\t') #ID
        New_ID_counter += 1
        
        output.write(','.join([r.id for r in sol['reactions']])+'\t') #Reactions
        output.write(str(sol['growth_rate'])+'\n') #Growth rate
        
        output.close()
