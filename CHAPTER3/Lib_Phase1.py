# -*- coding: utf-8 -*-
# ADAPTED FROM COBRAPY 0.8.0 gapfilling. The changes allow to preset the number of reaction additions and optimise for growth rate, rather than add a minimised number of reactions to obtain a predefined minimal growth rate.

from __future__ import absolute_import

from sympy import Add
from warnings import warn

from optlang.interface import OPTIMAL
from cobra.core import Model
from cobra.util import fix_objective_as_constraint

import Lib_General as Lib_g

class GapFiller(object):
    """Class for performing gap filling.

    This class implements gap filling based on a mixed-integer approach,
    very similar to that described in [1]_ and the 'no-growth but growth'
    part of [2]_ but with minor adjustments. In short, we add indicator
    variables for using the reactions in the universal model, z_i and then
    solve problem

    minimize \sum_i c_i * z_i
    s.t. Sv = 0
         v_o >= t
         lb_i <= v_i <= ub_i
         v_i = 0 if z_i = 0

    where lb, ub are the upper, lower flux bounds for reaction i, c_i is a
    cost parameter and the objective v_o is greater than the lower bound t.
    The default costs are 1 for reactions from the universal model, 100 for
    exchange (uptake) reactions added and 1 for added demand reactions.

    Note that this is a mixed-integer linear program and as such will
    expensive to solve for large models. Consider using alternatives [3]_
    such as CORDA instead [4,5]_.

    Parameters
    ----------
    model : cobra.Model
        The model to perform gap filling on.
    universal : cobra.Model
        A universal model with reactions that can be used to complete the
        model.
    lower_bound : float
        The minimally accepted flux for the objective in the filled model.
    penalties : dict, None
        A dictionary with keys being 'universal' (all reactions included in
        the universal model), 'exchange' and 'demand' (all additionally
        added exchange and demand reactions) for the three reaction types.
        Can also have reaction identifiers for reaction specific costs.
        Defaults are 1, 100 and 1 respectively.
    integer_threshold : float
        The threshold at which a value is considered non-zero (aka
        integrality threshold). If gapfilled models fail to validate,
        you may want to lower this value.
    exchange_reactions : bool
        Consider adding exchange (uptake) reactions for all metabolites
        in the model.
    demand_reactions : bool
        Consider adding demand reactions for all metabolites.

    References
    ----------
    .. [1] Reed, Jennifer L., Trina R. Patel, Keri H. Chen, Andrew R. Joyce,
       Margaret K. Applebee, Christopher D. Herring, Olivia T. Bui, Eric M.
       Knight, Stephen S. Fong, and Bernhard O. Palsson. “Systems Approach
       to Refining Genome Annotation.” Proceedings of the National Academy
       of Sciences 103, no. 46 (2006): 17480–17484.

       [2] Kumar, Vinay Satish, and Costas D. Maranas. “GrowMatch: An
       Automated Method for Reconciling In Silico/In Vivo Growth
       Predictions.” Edited by Christos A. Ouzounis. PLoS Computational
       Biology 5, no. 3 (March 13, 2009): e1000308.
       doi:10.1371/journal.pcbi.1000308.

       [3] http://opencobra.github.io/cobrapy/tags/gapfilling/

       [4] Schultz, André, and Amina A. Qutub. “Reconstruction of
       Tissue-Specific Metabolic Networks Using CORDA.” Edited by Costas D.
       Maranas. PLOS Computational Biology 12, no. 3 (March 4, 2016):
       e1004808. doi:10.1371/journal.pcbi.1004808.

       [5] Diener, Christian https://github.com/cdiener/corda
     """

    def __init__(self, model, universal=None, lower_bound=0.05,
                 penalties=None, exchange_reactions=False,
                 demand_reactions=False, integer_threshold=1e-6):
        self.original_model = model
        self.lower_bound = lower_bound
        self.model = model.copy()
        tolerances = self.model.solver.configuration.tolerances
        tolerances.integrality = integer_threshold
        self.universal = universal.copy() if universal else Model('universal')
        self.penalties = dict(universal=1, exchange=100, demand=1)
        if penalties is not None:
            self.penalties.update(penalties)
        self.integer_threshold = integer_threshold
        self.indicators = list()
        self.costs = dict(universal=1, exchange=100, demand=1)
        self.extend_model(exchange_reactions, demand_reactions)
        #fix_objective_as_constraint(self.model, bound=lower_bound) --- not required

        #Remove dead-end reactions and metabolites
        self.remove_dead_ends()

        self.add_switches_and_objective()

    def extend_model(self, exchange_reactions=False, demand_reactions=False):
        """Extend gapfilling model.

        Add reactions from universal model and optionally exchange and
        demand reactions for all metabolites in the model to perform
        gapfilling on.

        Parameters
        ----------
        exchange_reactions : bool
            Consider adding exchange (uptake) reactions for all metabolites
            in the model.
        demand_reactions : bool
            Consider adding demand reactions for all metabolites.
        """
        for rxn in self.universal.reactions:
            rxn.gapfilling_type = 'universal'
        for met in self.model.metabolites:
            if exchange_reactions:
                rxn = self.universal.add_boundary(
                    met, type='exchange_smiley', lb=-1000, ub=0,
                    reaction_id='EX_{}'.format(met.id))
                rxn.gapfilling_type = 'exchange'
            if demand_reactions:
                rxn = self.universal.add_boundary(
                    met, type='demand_smiley', lb=0, ub=1000,
                    reaction_id='DM_{}'.format(met.id))
                rxn.gapfilling_type = 'demand'

        new_reactions = self.universal.reactions.query(
            lambda reaction: reaction not in self.model.reactions
        )
        self.model.add_reactions(new_reactions)

    def remove_dead_ends(self):
        """ Removes dead-end metabolites and reactions """

        while True:
            print 'Number of reactions %d' % (len(self.model.reactions))
            met_list = [m for m in self.model.metabolites if len(m.reactions)==1]
    
            if len(met_list)==0:
                break
    
            print 'Removing %d dead-end metabolites' % (len(met_list))
            self.model.remove_metabolites(met_list,destructive=True)
    
        print 'Switching to only consumable / producible metabolites'    
        while True:
            print 'Number of reactions %d' % (len(self.model.reactions))
            met_list = [m for m in self.model.metabolites if len(m.reactions)==1
                                                          or not any([r for r in self.model.reactions if m in r.metabolites and #Not producible
                                                                               ((r.upper_bound>0 and r.metabolites[m]>0) or
                                                                                (r.lower_bound<0 and r.metabolites[m]<0))])
                                                          or not any([r for r in self.model.reactions if m in r.metabolites and #Not consumible
                                                                               ((r.upper_bound>0 and r.metabolites[m]<0) or
                                                                                (r.lower_bound<0 and r.metabolites[m]>0))])]
    
            if len(met_list)==0:
                break
    
            print 'Removing %d dead-end metabolites' % (len(met_list))
            self.model.remove_metabolites(met_list,destructive=True)    

    def add_switches_and_objective(self):
        """ Update gapfilling model with switches and the indicator objective.
        """
        constraints = list()
        big_m = max(max(abs(b) for b in r.bounds)
                    for r in self.model.reactions)
        prob = self.model.problem
        for rxn in self.model.reactions:
            if not hasattr(rxn, 'gapfilling_type') or rxn.id.startswith('DM_'):
                continue
            indicator = prob.Variable(
                name='indicator_{}'.format(rxn.id), lb=0, ub=1, type='binary')
            if rxn.id in self.penalties:
                indicator.cost = self.penalties[rxn.id]
            else:
                indicator.cost = self.penalties[rxn.gapfilling_type]
            indicator.rxn_id = rxn.id
            self.indicators.append(indicator)

            # if z = 1 v_i is allowed non-zero
            # v_i - Mz <= 0   and   v_i + Mz >= 0
            constraint_lb = prob.Constraint(
                rxn.flux_expression - big_m * indicator, ub=0,
                name='constraint_lb_{}'.format(rxn.id), sloppy=True)
            constraint_ub = prob.Constraint(
                rxn.flux_expression + big_m * indicator, lb=0,
                name='constraint_ub_{}'.format(rxn.id), sloppy=True)

            constraints.extend([constraint_lb, constraint_ub])

        #Add constraint indicating maximum number of new reactions in use
        #added_reactions = SparsePair(ind=[rxn.id for rxn in self.model.reactions if hasattr(rxn, 'gapfilling_type')],
        #                             val=[1.0    for rxn in self.model.reactions if hasattr(rxn, 'gapfilling_type')])

        #Add constraint on number of added reactions
        constraint_num_reactions = self.model.problem.Constraint(Add(*self.indicators),lb=0,ub=0,name='max_reactions')
        constraints.append([constraint_num_reactions])

        self.model.add_cons_vars(self.indicators)
        self.model.add_cons_vars(constraints, sloppy=True)

        #self.model.objective = prob.Objective(
        #    Add(*self.indicators), direction='min')
        #self.update_costs()


    def fill(self, iterations=1,num_reactions=3,reset=True,outputfile='output2.txt'):
        """Perform the gapfilling by iteratively solving the model, updating
        the costs and recording the used reactions."""
        if reset:
            self.remove_integer_cuts()

        #Update number of reaction additions (in order, code crashes if lb>ub...)
        if num_reactions < self.model.constraints.max_reactions.lb:
            self.model.constraints.max_reactions.lb=num_reactions
            self.model.constraints.max_reactions.ub=num_reactions
        else:
            self.model.constraints.max_reactions.ub=num_reactions
            self.model.constraints.max_reactions.lb=num_reactions
        
        used_reactions = list()
        for i in range(iterations):
            sol = self.model.slim_optimize(error_value=None, message='gapfilling optimization failed')
            solution = {'growth_rate': sol, 'reactions': [self.model.reactions.get_by_id(ind.rxn_id) for ind in self.indicators if ind._get_primal() > self.integer_threshold]}
            if not self.validate(solution['reactions']):
                print 'No (more) solutions found. Try increasing num_reactions'
                return used_reactions
                #raise RuntimeError('failed to validate gapfilled model, '
                #                   'try lowering the integer_threshold')
            used_reactions.append(solution)

            #Write to file
            

            #Add integer cuts
            sol_ind = [ind for ind in self.indicators if ind._get_primal() > self.integer_threshold]
            int_cut = self.model.problem.Constraint(Add(*sol_ind),lb=0,ub=len(sol_ind)-0.5,name='int_cut_{}'.format(i))
            self.model.add_cons_vars(int_cut, sloppy=True)

        return used_reactions


    def validate(self, reactions):
        with self.original_model as model:
            model.add_reactions(reactions)
            model.slim_optimize()
            return (model.solver.status == OPTIMAL and
                    model.solver.objective.value >= self.lower_bound)

    def remove_integer_cuts(self):
        #
        self.model.remove_cons_vars([c for c in self.model.constraints if 'int_cut' in c.name])

def delete_overlap(model,  reference):
    #deletes reactions from reference which also occur in the model. Necessary because gapfilling algorithm cannot handle it
    #if a reaction occurs in both. 
    model.repair()
    reference.repair()
    counter = 0
    substract = 0
    for i in range(len(reference.reactions)):
        #I use for i in.. instead of for elem in because somehow the indexing is messed up after adding reactions
        #from reference to model (done in addcycle()), therefore for elem in.. skips added reactions
        reaction_unique = True
        for elm in model.reactions:
            if Lib_g.check_samereaction(reference.reactions[i-substract],  elm):
                reaction_unique = False
        if reaction_unique == False or reference.reactions[i-substract] in model.reactions:
            reference.reactions[i-substract].remove_from_model()
            substract += 1
            counter += 1
    print counter,  'reactions removed from reference'
    return
